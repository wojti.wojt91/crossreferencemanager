# Generated by Django 3.0.3 on 2020-04-15 12:12

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('references_manager', '0011_auto_20200414_1520'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='company',
            options={'ordering': ['name']},
        ),
        migrations.AddField(
            model_name='product',
            name='type_image',
            field=models.ImageField(blank=True, upload_to=''),
        ),
    ]
