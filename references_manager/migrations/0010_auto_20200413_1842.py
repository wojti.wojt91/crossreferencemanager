# Generated by Django 3.0.3 on 2020-04-13 16:42

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('references_manager', '0009_auto_20200409_1915'),
    ]

    operations = [
        migrations.AlterField(
            model_name='file',
            name='file',
            field=models.FileField(blank=True, upload_to='files/'),
        ),
        migrations.AlterField(
            model_name='file',
            name='product',
            field=models.ForeignKey(blank=True, null=True, on_delete=django.db.models.deletion.CASCADE, related_name='files', to='references_manager.Product'),
        ),
        migrations.AlterField(
            model_name='reference',
            name='company',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='references', to='references_manager.Company'),
        ),
        migrations.AlterField(
            model_name='reference',
            name='product',
            field=models.ForeignKey(blank=True, on_delete=django.db.models.deletion.CASCADE, related_name='references', to='references_manager.Product'),
        ),
    ]
