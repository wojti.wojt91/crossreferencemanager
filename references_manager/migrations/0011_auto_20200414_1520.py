# Generated by Django 3.0.3 on 2020-04-14 13:20

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('references_manager', '0010_auto_20200413_1842'),
    ]

    operations = [
        migrations.AlterField(
            model_name='reference',
            name='company',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='references', to='references_manager.Company'),
        ),
        migrations.AlterField(
            model_name='reference',
            name='product',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='references', to='references_manager.Product'),
        ),
    ]
