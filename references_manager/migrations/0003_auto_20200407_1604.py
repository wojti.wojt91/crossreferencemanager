# Generated by Django 3.0.3 on 2020-04-07 14:04

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('references_manager', '0002_auto_20200407_1604'),
    ]

    operations = [
        migrations.RenameField(
            model_name='product',
            old_name='type_and_style',
            new_name='type',
        ),
    ]
