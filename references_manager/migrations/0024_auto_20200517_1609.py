# Generated by Django 3.0.3 on 2020-05-17 14:09

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('references_manager', '0023_auto_20200517_1605'),
    ]

    operations = [
        migrations.AlterField(
            model_name='product',
            name='a_in_inches',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=100, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='a_in_mm',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=100, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='b_in_inches',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=100, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='b_in_mm',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=100, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='c_in_inches',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=100, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='c_in_mm',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=100, null=True),
        ),
        migrations.AlterField(
            model_name='product',
            name='weight',
            field=models.DecimalField(blank=True, decimal_places=2, max_digits=100, null=True),
        ),
    ]
