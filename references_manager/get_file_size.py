import os
from django.conf import settings


def get_file_size(file_path):
    si = [' KB', ' MB', ' GB', 'TB']
    # returns file size in Bytes and changes it to KBs by dividing by 1024
    file_size = os.path.getsize(settings.MEDIA_ROOT + '/' + file_path)/1024

    # for every item in si list it checks if the file_size is less than 1024
    # if yes, it returns the correct unit
    # if not, it divides file_size again by 1024 and repeats until it's less than 1024 and return correct unit
    for i in si:
        if file_size < 1024:
            two_after_comma = str(file_size).find('.')
            file_size = str(file_size)[0:(two_after_comma + 3)]
            return str(file_size) + i
        file_size = file_size/1024
