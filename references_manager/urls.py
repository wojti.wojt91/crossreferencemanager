from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='index-page'),
    path('add/', views.add_product, name='add-page'),
    path('add_company/', views.add_company, name='add-company-page'),
    path('edit_product/<int:pk>', views.edit_product, name='edit-product'),
    path('edit_company/<int:pk>', views.edit_company, name='edit-company'),
    path('delete_product/<int:pk>', views.delete_product, name='delete-product'),
    path('delete_company/<int:pk>', views.delete_company, name='delete-company'),
    path('delete_file/<int:pk>', views.delete_file, name='delete-file'),
    path('delete_reference/<int:pk>', views.delete_reference, name='delete-reference'),
    path('search/', views.search, name='search-object'),
    path('detail/<int:pk>', views.detail, name='detail-page'),
    path('edit_reference/<int:pk>', views.edit_reference, name='edit-reference'),
    path('search_by_dimension/', views.search_by_dimension_template, name='search-dim-form'),
    path('search_by_dimension_results/', views.search_by_dimension, name='search-by-dimension'),
    path('company_list/', views.company_list, name='company-list'),
    path('company_list/company_detail/<int:pk>', views.company_detail, name='detail-company'),
    path('download/<int:pk>', views.serve_file, name='download'),
]
