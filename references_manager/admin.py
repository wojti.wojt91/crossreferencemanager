from django.contrib import admin
from .models import Product, Reference, Company, Type, File


admin.site.register([Product, Reference, Company, Type, File])

