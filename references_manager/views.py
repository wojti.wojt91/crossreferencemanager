from django.shortcuts import render, redirect, HttpResponse
from django.db.models import Q
from django.contrib import messages
from django.forms import modelformset_factory
from django.core.paginator import Paginator
from django.http import FileResponse
from django.contrib.auth.decorators import login_required
from django.conf import settings
from .forms import ProductForm, CompanyForm, RefEditForm, FileForm, RefForm
from .models import Reference, Product, Company, File, Type
from .clear_name import remove_unwanted_characters
from .get_max_or_min import get_max, get_min

# -------------------------------- ADD SECTION --------------------------------
# add product, references and files at once
@login_required
def add_product(request):
    if request.user.is_superuser:
        # create formsets for files and references
        file_formset = modelformset_factory(File, form=FileForm, fields=('file',), extra=4)
        reference_formset = modelformset_factory(Reference, form=RefForm, fields=('name', 'company'), extra=15)
        if request.method == 'POST':
            # if request method is POST it creates objects in db
            p_form = ProductForm(request.POST, request.FILES)
            formset_f = file_formset(request.POST, request.FILES, queryset=File.objects.none(), prefix='files')
            formset_r = reference_formset(request.POST, queryset=Reference.objects.none(), prefix='references')
            if p_form.is_valid() and formset_f.is_valid() and formset_r.is_valid():
                p_form.save()
                files = formset_f.save(commit=False)
                for file in files:
                    file.product_id = p_form.instance.id
                    file.save()
                references = formset_r.save(commit=False)
                for reference in references:
                    reference.product_id = p_form.instance.id
                    reference.save()
                # get product_name from cleaned data to show message on page
                product_name = p_form.cleaned_data.get('name')
                messages.success(request, f'Product "{product_name}" successfully added!')
                return redirect('detail-page', pk=p_form.instance.id)
            # if company fields are empty show error message on page
            messages.warning(request, f'Please choose a company to all new references')
        # if request method is not post create empty forms
        p_form = ProductForm()
        formset_f = file_formset(queryset=File.objects.none(), prefix='files')
        formset_r = reference_formset(queryset=Reference.objects.none(), prefix='references')
        return render(request, 'references_manager/add.html', {'p_form': p_form,
                                                               'formset_f': formset_f,
                                                               'formset_r': formset_r})
    else:
        # if user is not super user and somehow got a link to admins only page it returns permission denied template
        return render(request, 'references_manager/permission_denied.html')


@login_required
def add_company(request):
    if request.user.is_superuser:
        if request.method == "POST":
            company_form = CompanyForm(request.POST)
            if company_form.is_valid():
                company_form.save()
                return redirect('index-page')
        company_form = CompanyForm()
        return render(request, 'references_manager/add_company.html', {'company_form': company_form})
    else:
        return render(request, 'references_manager/permission_denied.html')

# -------------------------------- EDIT SECTION --------------------------------
# edit is same as adding but returns values of product fields
@login_required
def edit_product(request, pk):
    if request.user.is_superuser:
        product = Product.objects.filter(id=pk)
        if product:
            product = Product.objects.get(id=pk)
            file_formset = modelformset_factory(File, form=FileForm, fields=('file',), extra=4)
            reference_formset = modelformset_factory(Reference, form=RefForm, fields=('name', 'company'), extra=15)
            if request.method == 'POST':
                p_form = ProductForm(request.POST, request.FILES, instance=product)
                formset_f = file_formset(request.POST, request.FILES, queryset=File.objects.none(), prefix='files')
                formset_r = reference_formset(request.POST, queryset=Reference.objects.none(), prefix='references')
                if p_form.is_valid() and formset_f.is_valid() and formset_r.is_valid():
                    p_form.save()
                    files = formset_f.save(commit=False)
                    for file in files:
                        file.product_id = p_form.instance.id
                        file.save()
                    references = formset_r.save(commit=False)
                    for reference in references:
                        reference.product_id = p_form.instance.id
                        reference.save()
                    product_name = p_form.cleaned_data.get('name')
                    messages.success(request, f'Product "{product_name}" successfully edited!')
                    return redirect('detail-page', pk=product.id)
                messages.warning(request, f'Please choose a company to all new or edited references')

            p_form = ProductForm(instance=product)
            formset_f = file_formset(queryset=File.objects.none(), prefix='files')
            formset_r = reference_formset(queryset=Reference.objects.none(), prefix='references')
            return render(request, 'references_manager/edit2.html', {'p_form': p_form,
                                                                     'formset_f': formset_f,
                                                                     'formset_r': formset_r,
                                                                     'product': product})
        else:
            return HttpResponse('404')
    else:
        return render(request, 'references_manager/permission_denied.html')


@login_required
def edit_reference(request, pk):
    if request.user.is_superuser:
        reference = Reference.objects.filter(id=pk)
        if reference:
            reference = Reference.objects.get(id=pk)
            parent_product = reference.product_id
            if request.method == "POST":
                reference_form = RefEditForm(request.POST, instance=reference)
                if reference_form.is_valid():
                    reference_form.save()
                    messages.success(request, f'Reference "{reference.name}" successfully edited!')
                    return redirect('detail-page', pk=parent_product)
            else:
                reference_form = RefEditForm(instance=reference)
                return render(request, 'references_manager/edit_reference.html', {
                    'reference_form': reference_form,
                    'reference_instance': reference
                    })
        else:
            return HttpResponse('404')
    else:
        return render(request, 'references_manager/permission_denied.html')


@login_required
def edit_company(request, pk):
    if request.user.is_superuser:
        company = Company.objects.filter(id=pk)
        if company:
            company = Company.objects.get(id=pk)
            if request.method == "POST":
                company_form = CompanyForm(request.POST, instance=company)
                if company_form.is_valid():
                    company_form.save()
                    return redirect('index-page')
            else:
                company_form = CompanyForm(instance=company)
                return render(request, 'references_manager/edit_company.html', {
                    'company_form': company_form,
                    'company_instance': company
                    })
        else:
            return HttpResponse('404')
    else:
        return render(request, 'references_manager/permission_denied.html')


# -------------------------------- RENDER SECTION --------------------------------
# index page with pagination and info about objects in db
@login_required
def index(request):
    product_list = Product.objects.all().order_by('-id')
    paginator = Paginator(product_list, 20)
    product_count = Product.objects.count()
    company_count = Company.objects.count()
    reference_count = Reference.objects.count()

    page_number = request.GET.get('page')
    page_obj = paginator.get_page(page_number)
    return render(request, 'references_manager/index2.html', {'page_obj': page_obj,
                                                              'product_count': product_count,
                                                              'company_count': company_count,
                                                              'reference_count': reference_count})


# product details view
@login_required
def detail(request, pk):
    product = Product.objects.filter(id=pk)
    if product:
        product = Product.objects.get(id=pk)
        references = product.references.all().order_by('company')
        return render(request, 'references_manager/detail.html', {'product': product,
                                                                  'references': references})
    else:
        return HttpResponse('404')


@login_required
def company_list(request):
    companies = Company.objects.all()
    return render(request, 'references_manager/company_list.html', {'companies': companies})


@login_required
def company_detail(request, pk):
    company = Company.objects.filter(id=pk)
    if company:
        company = Company.objects.get(id=pk)
        company_references = company.references.all().order_by('name')
        return render(request, 'references_manager/company_detail.html', {'company': company,
                                                                          'company_references': company_references})
    else:
        return HttpResponse('404')


# -------------------------------- DELETE SECTION --------------------------------
@login_required
def delete_product(request, pk):
    if request.user.is_superuser:
        product = Product.objects.filter(id=pk)
        if product:
            query = Product.objects.get(id=pk)
            name = query.name
            messages.success(request, f'Product "{name}" successfully deleted!')
            query.delete()
            return redirect('index-page')
        messages.warning(request, f'Object does not exist.')
        return redirect('index-page')
    else:
        return render(request, 'references_manager/permission_denied.html')


@login_required
def delete_file(request, pk):
    if request.user.is_superuser:
        file = File.objects.filter(id=pk)
        if file:
            obj = File.objects.get(id=pk)
            parent_product = obj.product_id
            name = obj.name
            messages.success(request, f'File "{name}" successfully deleted!')
            obj.delete()
            return redirect('detail-page', pk=parent_product)
        messages.warning(request, f'Object does not exist.')
        return redirect('index-page')
    else:
        return render(request, 'references_manager/permission_denied.html')


@login_required
def delete_company(request, pk):
    if request.user.is_superuser:
        company = Company.objects.filter(id=pk)
        if company:
            query = Company.objects.get(id=pk)
            name = query.name
            messages.success(request, f'Company "{name}" successfully deleted!')
            query.delete()
            return redirect('index-page')
        messages.warning(request, f'Object does not exist.')
        return redirect('index-page')
    else:
        return render(request, 'references_manager/permission_denied.html')


@login_required
def delete_reference(request, pk):
    if request.user.is_superuser:
        reference = Reference.objects.filter(id=pk)
        if reference:
            query = Reference.objects.get(id=pk)
            parent_product = query.product_id
            name = query.name
            messages.success(request, f'Reference "{name}" successfully deleted!')
            query.delete()
            return redirect('detail-page', pk=parent_product)
        messages.warning(request, f'Object does not exist.')
        return redirect('index-page')
    else:
        return render(request, 'references_manager/permission_denied.html')

# -------------------------------- SEARCH SECTION --------------------------------
# search by name function
@login_required
def search(request):
    query = request.GET.get('q')
    if query:
        # clears query input from unwanted characters and search in clear_name field in Product and Reference objects
        clear_query = remove_unwanted_characters(name_to_clear=query)
        results_reference = Reference.objects.filter(
            Q(name__icontains=clear_query) | Q(clear_name__icontains=clear_query))
        results_product = Product.objects.filter(Q(name__icontains=clear_query) | Q(clear_name__icontains=clear_query))
        # appends results list with products
        results = []
        for product in results_product:
            if product not in results:
                results.append(product)
        # gets parent product of reference query results and checks if product is already in results list
        # if not it appends results list
        for product in results_reference:
            product = Product.objects.get(id=product.product_id)
            if product not in results:
                results.append(product)
        # it there is no results it gives message on page
        if not results:
            messages.info(request, f'Nothing found for: "{query}"')
            return redirect('index-page')
        if len(results) > 1:
            one_or_more = 'results'
        else:
            one_or_more = 'result'
        # message about results
        messages.success(request, f'Found {len(results)} {one_or_more} for: "{query}"')
        return render(request, 'references_manager/results2.html', {
            'results': results,
            'query': query,
            'results_product': results_product,
            'results_reference': results_reference,
        })
    else:
        return redirect('index-page')


@login_required
def search_by_dimension_template(request):
    types = Type.objects.all()
    return render(request, 'references_manager/search_by_dimension.html', {'types': types})

# search by dimension
@login_required
def search_by_dimension(request):
    # get all fields and type chosen from website
    a_min = request.GET.get('a_min')
    a_max = request.GET.get('a_max')
    b_min = request.GET.get('b_min')
    b_max = request.GET.get('b_max')
    c_min = request.GET.get('c_min')
    c_max = request.GET.get('c_max')
    type_chosen = request.GET.get('types')
    types = Type.objects.all()
    # if at least one field is filled it's searching for query
    if a_max or b_max or c_max or a_min or b_min or c_min:
        # search in ranges from form
        # changing empty form fields for min and max value with functions in get_max_or_min.py file
        results_mm = Product.objects.filter(
            Q(a_in_mm__range=(get_min(query=a_min), get_max(query=a_max))) &
            Q(b_in_mm__range=(get_min(query=b_min), get_max(query=b_max))) &
            Q(c_in_mm__range=(get_min(query=c_min), get_max(query=c_max))))
        results_inch = Product.objects.filter(
            Q(a_in_inches__range=(get_min(query=a_min), get_max(query=a_max))) &
            Q(b_in_inches__range=(get_min(query=b_min), get_max(query=b_max))) &
            Q(c_in_inches__range=(get_min(query=c_min), get_max(query=c_max))))
        # filtering with type choosen, or return all types when none filter selected on page
        if type_chosen is not None:
            results_mm = results_mm.filter(type__name=type_chosen)
            results_inch = results_inch.filter(type__name=type_chosen)
        # message if nothing found in dimension ranges
        if not results_mm:
            if not results_inch:
                messages.info(request, f'Nothing found ')
                return redirect('search-dim-form')

        return render(request, 'references_manager/dimension_results.html', {
            'results_mm': results_mm,
            'results_inch': results_inch,
            'count_mm': len(results_mm),
            'count_inch': len(results_inch),
            'types': types,
        })
    else:
        return redirect('index-page')

# -------------------------------- DOWNLOAD SECTION --------------------------------
# function to serve file as a attachment instead of direct link to file on server, preventing downloading without login
@login_required
def serve_file(request, pk):
    file_obj = File.objects.filter(id=pk)
    if file_obj:
        file_obj = File.objects.get(id=pk)
        return FileResponse(open((settings.MEDIA_ROOT + '/' + str(file_obj.file)), 'rb'), as_attachment=True)
    else:
        return HttpResponse('404')
