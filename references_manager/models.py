from django.db import models
from .clear_name import remove_unwanted_characters
from .get_file_size import get_file_size


class Type(models.Model):
    name = models.CharField(max_length=200, blank=False)
    image = models.ImageField(upload_to='', blank=True)

    def __str__(self):
        return self.name


class Product(models.Model):
    name = models.CharField(max_length=200, blank=False)
    clear_name = models.CharField(max_length=200, blank=True)

    a_in_mm = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, default=0)
    a_in_inches = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, default=0)
    b_in_mm = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, default=0)
    b_in_inches = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, default=0)
    c_in_mm = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, default=0)
    c_in_inches = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, default=0)
    weight = models.DecimalField(max_digits=10, decimal_places=2, blank=True, null=True, default=0)

    type = models.ForeignKey(Type, on_delete=models.SET_NULL, related_name='types', null=True)
    additional_info = models.TextField(blank=True)
    image = models.ImageField(upload_to='img/', default='default.png', blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # get clear name without unwanted characters
        original_name = self.name
        self.clear_name = remove_unwanted_characters(name_to_clear=str(original_name))
        # set 0 instead None for DecimalFields. (if user left empty field on form it returns None)
        if self.a_in_mm is None:
            self.a_in_mm = 0
        if self.a_in_inches is None:
            self.a_in_inches = 0
        if self.b_in_mm is None:
            self.b_in_mm = 0
        if self.b_in_inches is None:
            self.b_in_inches = 0
        if self.c_in_mm is None:
            self.c_in_mm = 0
        if self.c_in_inches is None:
            self.c_in_inches = 0
        if self.weight is None:
            self.weight = 0
        super(Product, self).save(*args, **kwargs)

    def update(self, *args, **kwargs):
        original_name = self.name
        self.clear_name = remove_unwanted_characters(name_to_clear=str(original_name))
        super(Product, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        # deletes files from disk space when deleting object
        file_objects = File.objects.filter(product=self)
        for obj in file_objects:
            obj.file.delete(save=False)
        
        if str(self.image) != 'default.png':
            print('not default')
            self.image.delete(save=False)
        super(Product, self).delete(*args, **kwargs)


class Company(models.Model):
    name = models.CharField(max_length=200, blank=False)

    # order by name on default
    class Meta:
        ordering = ['name']

    def __str__(self):
        return self.name


class Reference(models.Model):
    name = models.CharField(max_length=200, blank=True)
    clear_name = models.CharField(max_length=200, blank=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='references', blank=False)
    company = models.ForeignKey(Company, on_delete=models.CASCADE, related_name='references', blank=False)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        original_name = self.name
        self.clear_name = remove_unwanted_characters(name_to_clear=str(original_name))
        super(Reference, self).save(*args, **kwargs)

    def update(self, *args, **kwargs):
        original_name = self.name
        self.clear_name = remove_unwanted_characters(name_to_clear=str(original_name))
        super(Reference, self).save(*args, **kwargs)


class File(models.Model):
    name = models.CharField(max_length=100, blank=True)
    file = models.FileField(upload_to='files/', blank=True)
    file_size = models.CharField(max_length=300, blank=True)
    product = models.ForeignKey(Product, on_delete=models.CASCADE, related_name='files', null=True, blank=True)

    def __str__(self):
        return self.name

    def save(self, *args, **kwargs):
        # get name of file with extension from file path
        original_name = str(self.file)
        self.name = original_name.split('/')[-1]
        super(File, self).save(*args, **kwargs)
        # get file size with function from get_file_size.py file
        path = str(self.file)
        self.file_size = get_file_size(file_path=path)
        super(File, self).save(*args, **kwargs)

    def delete(self, *args, **kwargs):
        # delete the file
        self.file.delete(save=False)
        # delete the object
        super(File, self).delete(*args, **kwargs)

