# function to clear unwanted characters to fill 'clear_name' field

def remove_unwanted_characters(name_to_clear):

    unwanted_characters = ['`', '~', '!', '@', '#', '$', '%', '^', '&', '*', '(', ')', '-', '_', '=', '+',
                           '[', '{', '}', ']', '|', '\'', ':', ';', "'", '"', '<', ',', '>', '.', '?', '/', ' ']

    for character in unwanted_characters:
        name_to_clear = name_to_clear.replace(character, '')

    return name_to_clear
