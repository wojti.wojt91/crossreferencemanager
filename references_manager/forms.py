from django import forms
from .models import Product, Company, Reference, File
from crispy_forms.helper import FormHelper


class ProductForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ('name',
                  'image',
                  'a_in_mm',
                  'b_in_mm',
                  'c_in_mm',
                  'a_in_inches',
                  'b_in_inches',
                  'c_in_inches',
                  'weight',
                  'type',
                  'additional_info',)

    def __init__(self, *args, **kwargs):
        super(ProductForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control-sm'


class CompanyForm(forms.ModelForm):
    class Meta:
        model = Company
        fields = ('name',)


class RefForm(forms.ModelForm):
    class Meta:
        model = Reference
        fields = ('name',)

    def __init__(self, *args, **kwargs):
        super(RefForm, self).__init__(*args, **kwargs)
        for visible in self.visible_fields():
            visible.field.widget.attrs['class'] = 'form-control-sm'


class ReferenceForm(forms.Form):

    def __init__(self, *args, **kwargs):
        companies = Company.objects.all().order_by('id')
        super(ReferenceForm, self).__init__(*args, **kwargs)
        for i in companies:
            self.fields[i.name] = forms.CharField(label=i, required=False,)


class ReferenceEditForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(ReferenceEditForm, self).__init__(*args, **kwargs)
        self.helper = FormHelper()
        self.helper.form_show_labels = False

    class Meta:
        model = Reference
        fields = ('name',)


class ProductEditForm(forms.ModelForm):
    class Meta:
        model = Product
        fields = ('name',)


class RefEditForm(forms.ModelForm):

    class Meta:
        model = Reference
        fields = ('name',
                  'product',
                  'company')


class FileForm(forms.ModelForm):
    class Meta:
        model = File
        fields = ('file',)

    def __init__(self, *args, **kwargs):
        super(FileForm, self).__init__(*args, **kwargs)
        for field in self.fields:
            self.fields[field].widget.attrs = {'class': 'form-control'}

