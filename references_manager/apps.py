from django.apps import AppConfig


class ReferencesManagerConfig(AppConfig):
    name = 'references_manager'
