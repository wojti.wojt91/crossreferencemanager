# try to get float from search in dimension query
# if it's impossible (because field was empty) return min 0 or max infinite

def get_min(query):
    try:
        float(query)
        return float(query)
    except:
        return 0


def get_max(query):
    try:
        float(query)
        return float(query)
    except:
        return float('inf')
